CREATE ISO
=========

Here you will make changes to the following file for a custom iso. The ks.cfg file which is the kickstart file can be customized. The file is found in the following location files/rhel7/ks.cfg


Requirements
------------
This is a default install of RHEL 7.6 with gui. This is just a base line install with a user called someadmin. If you make any changes to ks.cfg the file will be checked for errors using ksvalidator. 


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------
Tameika Reed
